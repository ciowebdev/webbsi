import { Injectable } from '@angular/core'
import { SideChat } from './side-chat.model';

let date = new Date(),
    day = date.getDate(),
    month = date.getMonth(),
    year = date.getFullYear(),
    hour = date.getHours(),
    minute = date.getMinutes();

let chats = [
    new SideChat(
        'assets/img/users/default-user.jpg',
        'Demo Demo', 
        'Online',
        'Привет, я демо бот',
        new Date(year, month, day-2, hour, minute),
        'left'
    ),
    new SideChat(
        'assets/img/users/default-user.jpg',
        'Владимир Алексеев',
        'Do not disturb',
        'Привет. я занят. Это текст для демо',
        new Date(year, month, day-2, hour, minute),
        'left'
    )
]

let talks = [
    new SideChat(
        'assets/img/users/default-user.jpg',
        'Demo Demo', 
        'Online',
        'Привет, нужна от тебя помощь. Нпиши как сможешь...',
        new Date(year, month, day-2, hour, minute+2),
        'right'
    ),    
    new SideChat(
        'assets/img/users/default-user.jpg',
        'Владимир Алексеев', 
        'Do not disturb',
        'Как толкьо освобожксь - напишу',
        new Date(year, month, day-2, hour, minute+3),
        'left'
    ),
]

@Injectable()
export class SideChatService {

    public getChats():Array<Object> {
        return chats;
    }

    public getTalk():Array<Object> {
        return talks;
    }

}

















// import {Injectable} from '@angular/core'

// let date = new Date(),
//     day = date.getDate(),
//     month = date.getMonth(),
//     year = date.getFullYear(),
//     hour = date.getHours(),
//     minute = date.getMinutes();

// @Injectable()
// export class SideChatService {
    

//     private chats = [
//         {
//             image: 'assets/img/profile/ashley.jpg',
//             author: 'Ashley Ahlberg', 
//             authorStatus: 'Online',
//             text: 'Hi, I\'m looking for admin template with bootstrap 4.  What do you think about StartNG Admin Template?',
//             date: new Date(year, month, day-2, hour, minute),
//             time: '1 min ago'
//         },
//         {
//             image: 'assets/img/profile/bruno.jpg',
//             author: 'Bruno Vespa', 
//             authorStatus: 'Do not disturb',
//             text: 'Hi, I\'m looking for admin template with bootstrap 4.  What do you think about StartNG Admin Template?',
//             date: new Date(year, month, day-2, hour, minute),
//             time: '1 min ago'
//         },
//         {
//             image: 'assets/img/profile/julia.jpg',
//             author: 'Julia Aniston', 
//             authorStatus: 'Away',
//             text: 'Hi, I\'m looking for admin template with bootstrap 4.  What do you think about StartNG Admin Template?',
//             date: new Date(year, month, day-2, hour, minute),
//             time: '1 min ago'
//         },
//         {
//             image: 'assets/img/users/default-user.jpg',
//             author: 'unknown',
//             authorStatus: 'Offline',
//             text: 'After you get up and running, you can place Font Awesome icons just about...',
//             time: '1 min ago'
//         },



//         // {
//         //     image: 'michael',
//         //     author: 'Michael Blair',
//         //     text: 'You asked, Font Awesome delivers with 40 shiny new icons in version 4.2.',
//         //     time: '2 hrs ago'
//         // },
//         // {
//         //     image: 'julia',
//         //     author: 'Julia Aniston',
//         //     text: 'Want to request new icons? Here\'s how. Need vectors or want to use on the...',
//         //     time: '10 hrs ago'
//         // },
//         // {
//         //     image: 'bruno',
//         //     author: 'Bruno Vespa',
//         //     text: 'Explore your passions and discover new ones by getting involved. Stretch your...',
//         //     time: '1 day ago'
//         // },
//         // {
//         //     image: 'tereza',
//         //     author: 'Tereza Stiles',
//         //     text: 'Get to know who we are - from the inside out. From our history and culture, to the...',
//         //     time: '1 day ago'
//         // },
//         // {
//         //     image: 'adam',
//         //     author: 'Adam Sandler',
//         //     text: 'Need some support to reach your goals? Apply for scholarships across a variety of...',
//         //     time: '2 days ago'
//         // },

      
//     ]; 
//     public getChats():Array<Object> {
//         return this.chats;
//     }

// }