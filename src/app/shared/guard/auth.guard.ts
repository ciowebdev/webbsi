import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CookieService } from 'ngx-cookie';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private cookieService: CookieService,
    private router: Router) { }

  canActivate(): boolean {
    const authData: string = this.getCookie('authData');
    let authDataObject: any;
    console.log('Session SID: ', authData);

    if (typeof authData !== 'undefined') {
      authDataObject = JSON.parse(authData);
      if (authDataObject.user !== null) {
        console.log('Guard activate true...');
        return true;
      }
    }

    console.log('Guard activate false...');
    this.router.navigate(['/login']);
    return false;
  }

  private getCookie(key: string): string {
    return this.cookieService.get(key);
  }
}
