import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PagesComponent } from './pages.component';
import { BlankComponent } from './blank/blank.component';
import { SearchComponent } from './search/search.component';
import { GantComponent } from './gant/gant.component';
import { RepairComponent } from './repair/repair.component';

export const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: '', redirectTo: 'gant', pathMatch: 'full' },
            { path: 'gant', component: GantComponent, data: { breadcrumb: 'Gant' } },
            { path: 'repair', component: RepairComponent, data: { breadcrumb: 'Repair' } },
            { path: 'blank', component: BlankComponent, data: { breadcrumb: 'Blank page' } },
            { path: 'search', component: SearchComponent, data: { breadcrumb: 'Search' } }
       ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
