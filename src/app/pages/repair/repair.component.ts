import { Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/interval';

@Component({
  selector: 'app-repair',
  templateUrl: './repair.component.html',
  encapsulation: ViewEncapsulation.None
})

export class RepairComponent implements OnInit {

  rows = [];
  columns = [
    { prop: 'code', name: 'Код' },
    { prop: 'mcName', name: 'Рабочий центр' },
    { prop: 'start_date', name: 'Время начала' },
    { prop: 'end_date', name: 'Время окончания' },
    { prop: 'description', name: 'Описание' },
    { prop: 'stateName', name: 'Статус заявки' }
  ];
  public repairsList;
  public selected = [];

  constructor(private readonly http: Http) {

  }

  public ngOnInit() {
    this.http.get('http://10.2.0.33:8081/repairticket')
      .map((res) => res.json())
      .catch((err) => Observable.throw(err))
      .subscribe((res) => this.rows = res);

    Observable.interval(2000)
      // .timeInterval()
      .switchMap(() => this.http.get('http://10.2.0.33:8081/repairticket')
        .map(res => res.json()))
        .subscribe(data => {
          console.log(JSON.stringify(data));
          this.rows = data;
          if (this.rows.length > 0) {
            this.selected = [this.rows[0]];
          }
        });
  }

  public onSelect(e) {
    console.log(e);
  }
}
