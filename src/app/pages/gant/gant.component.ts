import { Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { IMultiSelectSettings, IMultiSelectTexts, IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-gant',
  templateUrl: './gant.component.html',
  encapsulation: ViewEncapsulation.None
})

export class GantComponent implements OnInit {
  editing = {};
  /* rows = []; */
  temp = [];
  selected = [];

  rows = [
    {
      'pf': 'ПФ-1842515 Тетради',
      'kp': 'КП-221842 Поліграф.посл.з друк.газ"Слобідський Край"№14',
      'dateStart': '20.02.2019 21:50:00',
      'dateEnd': '20.02.2019 22:20:00',
      'planQty': 0
    }
];
  columns = [
    { prop: 'pf', name: 'Обьект выпуска' },
    { prop: 'kp', name: 'КП' },
    { prop: 'dateStart', name: 'Время начала' },
    { prop: 'dateEnd', name: 'Время окончания' },
    { prop: 'planQty', name: 'Кол-во план' }
  ];
  settingsDisabled = false;
  taskDisabled = true;
  dashDisabled = true;
  settingsActive = true;
  taskActive = false;
  dashActive = false;
  public isWorkInProgress = true;
  public isShowCreateTicket = false;

  public mcs: any[];
  public selectedMC = '';
  public gants: any[];
  public inputQty = 0;
  public inputQtyTN = 0;
  public customDescr = '';
  public secondControlModel: number[];
  public secondControlSettings: IMultiSelectSettings = {
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-secondary btn-block',
    dynamicTitleMaxItems: 8,
    displayAllSelectedText: false,
    showCheckAll: true,
    showUncheckAll: true
  };
  public secondControlTexts: IMultiSelectTexts = {
    checkAll: 'Выбрать всех',
    uncheckAll: 'Очистить выбор',
    checked: 'выбран',
    checkedPlural: 'выбрано',
    searchPlaceholder: 'поиск..',
    defaultTitle: 'Выберите состав бригады...',
    allSelected: 'Выбраны все',
  };
  public secondControlOptions: IMultiSelectOption[] = [
    { id: 1, name: 'Радченко И.С.' },
    { id: 2, name: 'Новоселова В.А.' },
    { id: 3, name: 'Беспалюк Л.А.' },
    { id: 4, name: 'Бобров И.Д.' },
    { id: 5, name: 'Еромасов Е.П.' },
    { id: 6, name: 'Захаревский В.С.' },
    { id: 7, name: 'Какадий С.Л.' },
    { id: 8, name: 'Кузнецов Д.Л.' }
  ];

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private readonly http: Http) {
    this.selected = [{'planQty': 0}];


  }

  ngOnInit() {
    this.http.get('http://10.2.0.33:8081/mc')
      .map((res) => res.json())
      .catch((err) => Observable.throw(err))
      .subscribe((res) => this.mcs = res);

      this.http.get('http://10.2.0.33:8081/gant')
      .map((res) => res.json())
      .catch((err) => Observable.throw(err))
      .subscribe((res) => this.gants = res);
  }

  onMCChanged() {
    console.log(this.selectedMC);
    this.rows = this.gants.filter((el) => el.mc === this.selectedMC)
    .map((el) => {return {
      'pf': el.pf_name,
      'kp': el.name,
      'dateStart': el.start_time,
      'dateEnd': el.end_time,
      'planQty': el.quantity
    }})
    this.selected = [{'planQty': 0}];
    console.log(this.rows);
  }

  onQtyChange() {
    this.inputQtyTN = this.inputQty;
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  updateValue(event, cell, cellValue, row) {
    this.editing[row.$$index + '-' + cell] = false;
    this.rows[row.$$index][cell] = event.target.value;
  }

  onSelect({ selected }) {
    
    console.log('Select Event', selected, this.selected);
  }

  saveSettings() {
    this.settingsDisabled = false;
    this.taskDisabled = false;
    this.dashDisabled = true;
    this.settingsActive = false;
    this.taskActive = true;
    this.dashActive = false;
  }

  startWork() {
    this.taskDisabled = true;
    this.settingsDisabled = true;
    this.dashDisabled = false;
    this.settingsActive = false;
    this.taskActive = false;
    this.dashActive = true;
  }

  endWork() {
    this.taskDisabled = false;
    this.settingsDisabled = false;
    this.dashDisabled = true;
    this.settingsActive = false;
    this.taskActive = true;
    this.dashActive = false;
    this.inputQty = 0;
    this.inputQtyTN = 0;
  }

  public onChange() {
    console.log(this.secondControlModel);
  }

  public createRepairTicket() {
    this.isShowCreateTicket = true;
  }

  public continueWork() {
    this.isWorkInProgress = true;
  }

  public cancelCreateTicket() {
    this.isWorkInProgress = true;
    this.isShowCreateTicket = false;
  }

  public createTicket() {
    this.isShowCreateTicket = false;
    this.isWorkInProgress = false;
    this.http.post('http://10.2.0.33:8081/repairticket', {mc: this.selectedMC, descr: this.customDescr})
      .map((res) => res)
      .catch((err) => Observable.throw(err))
      .subscribe((res) => console.log(res));
    this.customDescr = '';
  }

}
