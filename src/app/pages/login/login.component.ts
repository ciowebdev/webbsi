import { Component, ViewEncapsulation, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, AfterViewInit {
  public router: Router;
  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;

  constructor(router: Router, fb: FormBuilder,
    private cookieService: CookieService) {
      this.router = router;
      this.form = fb.group({
          'email': ['', Validators.compose([Validators.required])],
          'password': ['', Validators.compose([Validators.required, Validators.minLength(3)])]
      });

      this.email = this.form.controls['email'];
      this.password = this.form.controls['password'];
  }

  public ngOnInit() {
    this.cookieService.remove('authData');
  }

  public onSubmit(values: any): void {
      if (this.form.valid) {
        console.log(values);
          this.cookieService.putObject('authData', { user: values.email });
          if (values.email === 'rem') {
              this.router.navigate(['/pages/repair']);
          } else {
              this.router.navigate(['pages/gant']);

          }
      }
  }

  public ngAfterViewInit() {
      document.getElementById('preloader').classList.add('hide');
  }

}
